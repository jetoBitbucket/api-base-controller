'use strict';

/**
 * Elpunto de entrada
 * @module api-base-controller
 **/ 
module.exports = require('./lib/baseController');