"use strcit";
const Loader = require('./loader');
const Response = require('./Response');
let $instances;
class BaseController {

    constructor(base_dir) {
        if (!base_dir) throw 'ERROR: base_dir not defined';
        if(!$instances){
		this._base_dir = base_dir;
        	this._load = new Loader(base_dir);
        	$instances = this;
	}
    }

    get load() {
        return $instances._load;
    }

    httpResponse(res){
        return new Response(res);
    }
}

module.exports =  BaseController;