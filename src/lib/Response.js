class Response {

    constructor(res){
        this.res = res;
        this._messages = {
            100: "Continue",
            101: "Switching Protocols",
            200: "OK",
            201: "Created",
            202: "Accepted",
            203: "Non-Authoritative Information",
            204: "No Content",
            205: "Reset Content",
            206: "Partial Content",
            300: "Multiple Choices",
            301: "Moved Permanently",
            302: "Found",
            303: "See Other",
            304: "Not Modified",
            305: "Use Proxy",
            307: "Temporary Redirect",
            400: "Bad Request",
            401: "Unauthorized",
            402: "Payment Required",
            403: "Forbidden",
            404: "Not Found",
            405: "Method Not Allowed",
            406: "Not Acceptable",
            407: "Proxy Authentication Required",
            408: "Request Time-out",
            409: "Conflict",
            410: "Gone",
            411: "Length Required",
            412: "Precondition Failed",
            413: "Request Entity Too Large",
            414: "Request-URI Too Large",
            415: "Unsupported Media Type",
            416: "Requested Range not Satisfiable",
            417: "Expectation Failed",
            422: "Unprocessable Entity",
            429: "Too Many Requests",
            500: "Internal Server Error",
            501: "Not Implemented",
            502: "Bad Gateway",
            503: "Service Unavailable",
            504: "Gateway Time-out",
            505: "HTTP Version not Supported",
            506:"Variant Also Negotiates",
            507:"Insufficient Storage (WebDAV)",
            508:"Loop Detected (WebDAV)",
            510: "Not Extended",
            511:"Network Authentication Required"
        };
    }

    /**
     * 
     * @param code
     * @param message
     * @param data
     * @param callback
     */
    send(code,message,data,callback){
        if(callback && typeof callback == 'Function') {
            /*let res = yield callback();
            res.status(code).send({
                code:code,
                messages:message? message : this._messages[code],
                data:code < 400 ? data : null,
                errors: code > 400 ? data : null
            });*/
        }else{
            this.res.status(code).send({
                code:code,
                messages:message? message : this._messages[code],
                data:code < 400 ? data : null,
                errors: code > 400 ? data : null
            }) 
        }
        
    }
}
module.exports = Response;
