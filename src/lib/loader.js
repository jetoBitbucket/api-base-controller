"use strict";
const path = require('path');
const fs = require('fs');
class Loader{
    
    constructor(base_dir,options){

        this._base_dir = base_dir;
        this._options = options || {};
        try {
            this._models = require(path.join(this._base_dir,'models'));
            this._configs = require(path.join(this._base_dir,'config'));
            this._libraries_base_dir = require(path.join(this._base_dir,'libraries'));
            this._libraries = require('./libraries');
            
        }catch (e){
            console.log(e);
            throw 'ERROR: folders not defined';
        }
    }

    /**
     * deveulve los modelos que se encuentra declarados dentro de la carpeta model
     *
     * @param name, nombre del modelo solicitado
     * @returns {*}
     */
    models(name){
        if(name)
            return this._models[name];
        return this._models;
        console.log('Get models');
    }

    /**
     * deveulve el contenido de los archivos declarados en la carpeta config
     *
     * @param name, nombre del archivo de configuracion
     * @param key, clave solicitada del archivo de configuracion
     * @returns {*}
     */
    config(name,key){
        if(!name) return this._configs;
        try{
            if(name && key) return this._configs[name][key]
            return this._configs[name];
            console.log('Get configs');
        }catch (e){
            console.log(e)
            return false;
        }
    }

    /**
     * deveulve las librerias que se encuentra declarados dentro de la carpeta libraries de la raiz del api o la carpeta libraries de
     * api-base-controller
     *
     * @param name
     * @param options
     * @returns {boolean}
     */
    libraries(name,options){
        let _opt = options || null;

        if(!name) throw 'Error: not found libraries name';

        if(this._libraries_base_dir[name] || this._libraries[name]){
            if(options == null){
                if(this._libraries_base_dir[name])  return new this._libraries_base_dir[name]();
                if(this._libraries[name])  return new this._libraries[name]()
            }else{
                if(this._libraries_base_dir[name])  return new this._libraries_base_dir[name](options);
                if(this._libraries[name])  return new this._libraries[name](options)
            }
        }
        return false
    }

    /**
     *
     * @param name
     */
    helpers(name){
        
    }

    /**
     * Verifica si un existe un directorio en especifico
     *
     * @param directory, ruta del directorio
     * @returns {boolean}
     * @private
     */
    _isDirectory(directory){
        var self = this;
        let stat = fs.lstatSync(directory);
        if ( stat.isDirectory() ) return true;
        return false;
    }   

}

module.exports = Loader;