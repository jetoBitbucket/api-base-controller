/**
 * Exporta todos las libraries para poder ser usados con require
 * @author Jefferson Lara <jefferson.lara@sigis.com.ve>
 */
var fs = require('fs'),
    path = require('path'),
    lib = {};

fs.readdirSync(__dirname)
    .filter(function (file) {
        return (file.indexOf('.') !== -1) && (file.indexOf('.') !== 0) && (file !== 'index.js')
    })
    .forEach(function (file) {
        var object = require(path.join(__dirname,file));
        var nameLib = file.substring(0,file.indexOf('.'));
        lib[nameLib] = object;
    })

module.exports = lib;