
class libExampleBasedir{

    constructor(options){
         this._options = options || {};
         this._name = this._options.name || 'Nombre por default';
    }

    init(){
        console.log('iniciando libExampleBasedir desde algun controlador')
    }

    get name(){
        return this._name;
    }
}

module.exports = libExampleBasedir;