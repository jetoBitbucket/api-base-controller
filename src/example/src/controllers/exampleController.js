/**
 * Clase controller example
 * @class exampleController
 * @module controllers
 */
"use strict";
const path = require('path');
const BaseController = require('../../../lib/baseController');

class exampleController extends BaseController{
    /**
     * IO Constructor
     */
    constructor(){
        super(path.join(__dirname,'../'))
    }
    /**
     * 
     * @method find
     * @param req
     * @param res
     */
    exampleBaseUse(req,res){

        var lib = this.load.libraries('libExampleBasedir'),
            configs = this.load.config();

        console.log('Informacion de los archivos de configuracion -->',configs)

        lib.init();

        var libOptions = this.load.libraries('libExampleBasedir',{name:'jefferson lara'});

        console.log('Parametro pasado a libExampleBaseDir->',libOptions.name);

        console.log('Parametro por default libExampleBaseDir->',lib.name);
        
        this.httpResponse(res).send(200,null,{name:lib.name});
        
    }
}

module.exports = new exampleController();
