API BASE CONTROLLER
===================
Breve descripción de la aplicación.

INSTALACIÓN
========

```

npm install npm install git+http://git.sigis.com.ve/jlara/api-base-controller.git --save 
```

USO
========
```
 
 /**
  * Clase controller example
  * @class exampleController
  * @module controllers
  */
 "use strict";
 const path = require('path');
 const BaseController = require('api-base-controller');
 
 class exampleController extends BaseController{
      /**
      * IO Constructor
      */
     constructor(){
         super(path.join(__dirname,'../'))
     }
 }
```

METODOS
=======

|  Nombre       | Description |
|---------------|-------------|
|  load         | Super objeto que contien los metodos necesario para interactuar con la estructura del marco de trabajo |
|  config       | metodo que te permite acceder a la informacion de los archivos de configuracion que se encuntran ubicado en la carpeta config, para ver ejemplo de uso [click aqui]() |
|  models       | metodo que te permite obtener un modelo especifico, o todos si los quieres, para ver ejemplos de uso[click aqui]()  |
|  libraries    | metodo que te instanciar u obtener el objeto de una libreria especifica, para ver ejemplos de uso[click aqui]()     |
|  httpResponse | Metod que maneja las respuesta del servidor, tomando en cuenta el codigo http para armar los mensages y permitiendo terner una estructura estandar para todas las respuesta en el api, para ver ejemplo de uso [click aqui]() |


AUTORES
========
Jefferson Arturo Lara 

COPYRIGHT
========
Copyright (c) SIGIS Soluciones Integrales GIS C.A
